import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import Header from "../../shared/header/Header";
import Home from "../../pages/home/Home";
import Students from "../../pages/students/Students";
import Footer from "../../shared/footer/Footer";
import LessonPage from "../../pages/lessons/LessonPage";
import CourseCrud from "../../pages/courses/CourseCrud"
import LessonPage2 from "../../pages/lessons/lesson2/LessonPage2";
import LessonPage3 from "../../pages/lessons/lesson3/LessonPage3";
import LessonPage4 from "../../pages/lessons/lesson4/LessonPage4";
import LessonPage5 from "../../pages/lessons/lesson5/LessonPage5";
import LessonPage6 from "../../pages/lessons/lesson6/LessonPage6";
import NewLesson from "../../pages/lessons/newLesson/NewLesson";

import "./MainLayout.scss";

export default class MainLayout extends Component {
  componentDidMount() {
    const token = localStorage.getItem("access_token");
 
    if (!token) {
      this.props.history.push("/register");
    }
  }

  render() {
    return (
      <div className="MainLayout">
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/users" component={Students} />
          <Route path="/posts" component={CourseCrud} />
          <Route path="/lessons" component={LessonPage} />
          <Route path="/lesson2" component={LessonPage2} />
          <Route path="/lesson3" component={LessonPage3} />
          <Route path="/lesson4" component={LessonPage4} />
          <Route path="/lesson5" component={LessonPage5} />
          <Route path="/lesson6" component={LessonPage6} />
          <Route path="/newLesson" component={NewLesson} />
        </Switch>
        <Footer />
      </div>
    );
  }
}
