import React, { Component } from "react";
import { Formik } from "formik";
import Input from "../../shared/input/Input";

import { connect } from "react-redux";
import { register } from "../../../services/auth";
import { authorize } from "../../../store/actions/auth.actions";

import "./RegisterLayout.scss";

class RegisterLayout extends Component {
  state = {
    formError: ""
  };

  handleSubmit = values => {
    const { email, password } = values;
    
    register(email, password)
      .then(response => {
        this.props.authorize(email, response.data.token);
        if(this.props.authorize){
          this.props.history.push("/");
      }
      })
      .catch(error => {
        if (error.response) {
          this.setState({ formError: error.response.data.error });
          
        } else {
          this.setState({ formError: error.message });
        }
      });    
  };

  LoginClick = event => {
    event.preventDefault();
    this.props.history.push("/auth")
}

  validateForm = values => {
    const errors = {};

    const emailRegExp = new RegExp(
      [
        '^(([^<>()[\\]\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\.,;:\\s@"]+)*)',
        '|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.',
        "[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+",
        "[a-zA-Z]{2,}))$"
      ].join("")
    );

    if (!values.email) {
      errors.email = "Заполните поле Email";
    } else if (!emailRegExp.test(values.email)) {
      errors.email = "Введите правильный email";
    }

    if (!values.password) {
      errors.password = "Заполните пароль";
    } 

    if (!values.rePassword) {
      errors.rePassword = "Повторите пароль";
    } else if (values.password !== values.rePassword) {
      errors.rePassword = "Пароли не совпадают";
    }

    return errors;
  };

  renderForm = ({
    handleSubmit,
    handleChange,
    errors,
    setFieldTouched,
    touched
  }) => (
    <form onSubmit={handleSubmit}>
      <Input
        name="email"
        type="text"
        placeholder="Введите логин (eve.holt@reqres.in)"
        onBlur={() => setFieldTouched("email")}
        error={errors.email}
        touched={touched.email}
        onChange={handleChange}
      />
      <Input
        name="password"
        type="password"
        placeholder="Введите пароль"
        onBlur={() => setFieldTouched("password")}
        error={errors.password}
        touched={touched.password}
        onChange={handleChange}
      />
      <Input
        name="rePassword"
        type="password"
        placeholder="Повторите пароль"
        onBlur={() => setFieldTouched("rePassword")}
        error={errors.rePassword}
        touched={touched.rePassword}
        onChange={handleChange}
      />
      {this.state.formError && (
        <p className="text--error">{this.state.formError}</p>
      )}
      <div className="Register__button">
      <h2>Зарегестрированы? &nbsp; <span onClick={this.LoginClick}>Войти</span></h2>
      <button className="button--blue"
      type="submit">Sign up</button>
      </div>
    </form>
  );

  render() {
    return (
      <div className="Register__form">
      <Formik
        onSubmit={this.handleSubmit}
        render={this.renderForm}
        validate={this.validateForm}
        initialValues={{
          email: "",
          password: "",
          rePassword: ""
        }}
      />
    </div>
    );
  }
}

export default connect(
  null,
  { authorize }
)(RegisterLayout);
