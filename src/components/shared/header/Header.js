import React  from "react";
import {connect} from "react-redux";
import { NavLink } from "react-router-dom";

import "./Header.scss";

 function Header() {
  const token = localStorage.getItem("access_token");
  const username = token ? token.split(":")[0] : "";
     return (
       <div className="Header">
         <div className="Header__link">
         <span className="logo">Dar
			<span className="lab">Lab</span>
		</span>
         </div>
         <NavLink className="Header__link" to="/users">
           Мои cтуденты
         </NavLink>
         <NavLink className="Header__link" to="/posts">
           Мои курсы
         </NavLink>
         <NavLink exact className="Header__link" to="/">
          {username} 
         </NavLink>
       </div>
     );
   }
   

   function mapStateToProps(state){
     return {
       email: state.user.email
     };
   }

   export default connect(
     mapStateToProps
   )(Header);

