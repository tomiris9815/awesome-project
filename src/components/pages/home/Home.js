import React from "react";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../../../store/actions/auth.actions";

import "./Home.scss";

function Home(props) {
  const handleClick = () => {
    props.logout();
    props.history.push("/register");
  }
  return (
    <div className="Home">
      <button className="button--blue" onClick={() => handleClick()} >Выйти</button>
    </div>
  );
}

export default connect(
  null,
  { logout }
)(Home);