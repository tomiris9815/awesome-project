import React, { Component } from "react";
import Settings from "../settings/Settings";

import { getStudentsList } from "../../../services/students";
import "./StudentsPage.scss";

class Students extends Component {
  state = {
    users: []
  };

  componentDidMount() {
    const token = localStorage.getItem("access_token");
    if (!token) {
      this.props.history.push("/register");
    } else {
      getStudentsList()
        .then(response => {
          this.setState({ users: response.data.data });
        })
        .catch();
    }
  }

  render() {
    // console.log(this.state.users);
    return (
      <div className="Students">
      <div className="Students__title">
      <b>Мои студенты</b>
       <div className="Students__back">
         <Settings />
         </div>
      </div>
       <div className="Students__list">
        {/* <button onClick={this.props.history.goBack}>Back</button> */}
        
        {this.state.users.map(user => (
          <p className="Student__single" key={user.id}>{user.first_name} &nbsp; {user.last_name}</p>
        ))}
      </div>
      </div>
    );
  }
}

export default Students;
