import React, { Component } from "react";
import Settings from "../settings/Settings";

import CourseList from "./CourseList/CourseList";
import "./CourseCrud.scss";

class CoursePage extends Component {
  state = {
    todos: [],
    somevalue: 456,
    act: 0,
    index: '',
  };

  addTodo = (e) =>{
    e.preventDefault();
    let todos = this.state.todos;
    let name = this.refs.name.value;

    if(this.state.act === 0){   
      let data = {
        name
      }
      todos.push(data);
     
    }else{                      
      let index = this.state.index;
      todos[index].name = name;   
    }    

    this.setState({
      todos: todos,
      act: 0
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  }
  
  deleteTodo = i => {
    this.setState(state => {
      state.todos.splice(i, 1);
      return { todos: state.todos };
    });
  };

  editTodo = (i) => {
    let data = this.state.todos[i];
    this.refs.name.value = data.name;

    this.setState({
      act: 1,
      index: i
    });

    this.refs.name.focus();
  } 
  
  toggleDone = i => {
    this.setState(state => {
      const { todos } = state;
      todos[i].done = !todos[i].done;

      return { todos };
    });
  };
  render() {
    const { todos } = this.state;
    return (
      <div className="CourseLayout">
        <div className="Students__title">
      <b>Мои курсы</b>
       <div className="Students__back">
         <Settings />
         </div>
      </div>
        <form ref="myForm" className="App__form" onSubmit={this.addTodo}>
          <input
          placeholder="Добавить новый урок"
            className="App__input"
            name="todo"
            ref="name"
          />
          <button className="App__form__button">Добавить</button>
        </form>
        <div className="App__todos">
          <CourseList
            list={todos}
            deleteTodo={this.deleteTodo}
            done={this.toggleDone}
            edit={this.editTodo}
          />
        </div>
      </div>
    );
  }
}

export default CoursePage;