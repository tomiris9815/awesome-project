import React from "react";
import { NavLink } from "react-router-dom";

import "./CourseList.scss";
export default function TodoList({ list, deleteTodo, done,edit}) {
  // ( props )
  return (
    <div className="TodoList w3-table w3-striped w3-bordered">     
        <div >
            <div className="TodoList__title">
               
                <div>Учебный курс по React </div>
                
            </div>
            <div className="TodoList__elem ">
                <div className="listLink">
                <NavLink  to="/lessons">
                <div className="listNumber"> Урок 1</div>
                <div>Знакомимся с React.js Стандарты ES6-8</div>
                </NavLink>
                </div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button"/>
                    <button  className="App__edit__button"  >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button">
                      <img className="editImage"alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div>
           
            <div className="TodoList__elem ">
                <div>
                <NavLink  to="/lesson2">
                <div className="listNumber">Урок 2</div>
                <div> Модульная структура, зачем она нужна в React.</div>
                </NavLink>
                </div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button"/>
                    <button  className="App__edit__button"  >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button">
                        <img className="editImage" alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div>
            <div className="TodoList__elem ">
                <div>
                <NavLink  to="/lesson3">
                <div className="listNumber">Урок 3</div>
                <div> Препроцессор JSX</div>
                </NavLink>
                </div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button"/>
                    <button  className="App__edit__button"  >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button">
                        <img className="editImage" alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div>
            <div className="TodoList__elem ">
                <div>
                <NavLink  to="/lesson4"> 
                <div className="listNumber">Урок 4</div>
                <div> Практика. Используем props и state</div>
                </NavLink>
                </div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button"/>
                    <button  className="App__edit__button"  >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button">
                        <img className="editImage" alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div>
            <div className="TodoList__elem ">
                <div>
                <NavLink  to="/lesson5">
                <div className="listNumber">Урок 5</div>
                <div> Свойства, состояния и события в React</div>
                </NavLink>
                </div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button"/>
                    <button  className="App__edit__button"  >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button">
                        <img className="editImage" alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div>
            <div className="TodoList__elem ">
                <div>
                <NavLink  to="/lesson6">
                <div className="listNumber">Урок 6</div>
                <div> Практика. Удаление и добавление элементов</div>
                </NavLink>
                </div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button"/>
                    <button  className="App__edit__button"  >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button">
                        <img className="editImage" alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div> 

            {list.map((todo, i) => (
            <div
                key={i}
                className={`TodoList__elem ${todo.done ? "TodoList__elem--done" : ""} `}
            >
                <div>
                <NavLink  to="/newLesson">
                <div className="listNumber"> Урок {6+i+1}</div>
                <div> {todo.name}</div>
                </NavLink>
                </div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button" onClick={() => done(i)}/>
                    <button  className="App__edit__button" onClick={()=>edit(i)} >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button"onClick={() => deleteTodo(i)}>
                        <img className="editImage" alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div>
            ))}
        </div>
    </div>
  );
}
