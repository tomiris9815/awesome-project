import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import "./Settings.scss";

class Settings extends Component {
  handleClick = () => {
    //some actions ...
    this.props.history.push("/");
  };

  render() {
    return (
      <div className="goBack">
        <span  onClick={this.handleClick}>Главная</span>
      </div>
    );
  }
}

export default withRouter(Settings);
