import React from "react";

export default function TodoList({ list, deleteTodo, done,edit}) {
  // ( props )
  return (
    <div className="Crud__background">
        
            {list.map((todo, i) => (
            <div
                key={i}
            >
                <div className="Crud__lecture">Лекция {1+i+1}</div>
                <div className="Crud__content"> {todo.name}</div>
                <div className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button" onClick={() => done(i)}/>
                    <button  className="App__edit__button" onClick={()=>edit(i)} >
                        <img className="editImage" alt= " " src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button"onClick={() => deleteTodo(i)}>
                        <img className="editImage" alt= " " src={require('./delete.png') }/>
                    </button>
                </div>
                </div>
            </div>
            ))}
        
        </div> 
  );
}
