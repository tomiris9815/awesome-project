import React,{Component} from "react";
import Settings from "../../settings/Settings";
//import { Link } from "react-router-dom";
import LessonList from "../LessonList/LessonList";

import "./LessonPage5.scss";

class  СrudPage extends Component {
  state = {
    todos: [],
    somevalue: 456,
    act: 0,
    index: '',
  };

  addTodo = (e) =>{
    e.preventDefault();
    let todos = this.state.todos;
    let name = this.refs.name.value;

    if(this.state.act === 0){   
      let data = {
        name
      }
      todos.push(data);
     
    }else{                      
      let index = this.state.index;
      todos[index].name = name;   
    }    

    this.setState({
      todos: todos,
      act: 0
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  }
  
  deleteTodo = i => {
    this.setState(state => {
      state.todos.splice(i, 1);
      return { todos: state.todos };
    });
  };

  editTodo = (i) => {
    let data = this.state.todos[i];
    this.refs.name.value = data.name;

    this.setState({
      act: 1,
      index: i
    });

    this.refs.name.focus();
  } 
  
  toggleDone = i => {
    this.setState(state => {
      const { todos } = state;
      todos[i].done = !todos[i].done;

      return { todos };
    });
  };

  render(){ 
    const { todos } = this.state;
  return (
    <div className="Crud">
       <div className="Students__title">
      <b>Мои лекции</b>
       <div className="Students__back">
         <Settings />
        </div>
      </div>
      <div className="Crud__background">
      <div className="Crud__title">Урок 5 - Знакомимся с React.js Стандарты ES6-8</div>
      <div className="Crud__lecture">Лекция 1</div>
      <div className="Crud__content"> <b>Дополнительные ссылки:</b> 
     <br/>
     •	React.Fragments:  https://reactjs.org/docs/fragments.html
<br/>
Bootstrap:
•	https://www.bootstrapcdn.com/
<br/>
•	Class Fields:  КЛИК
<br/>
•	Binding events:  КЛИК
<br/>

        <b>  Задание <br/>ОБЯЗАТЕЛЬНОЕ ЗАДАНИЕ: </b>
        <br/>
        1. Написать условие, что если в данных, которые приходят от сервера, 
        есть что-то кроме объектов - они игнорируются. (Если в массив добавить число - пустой пост на его месте не создается).
        <br/>
2. Опционально: стилизовать проект на свой вкус.
<br/> 
3. Создайте коммит с номером урока и уровнем задания,
 запуште GitHub. Делаем все в одной папке.
 <br/>

          <b>УСЛОЖНЕННОЕ ЗАДАНИЕ:  </b>
          1. Добавить кнопку редактирования поста. 
<br/>2. Добавить форму редактирования поста. Изначально она скрыта и появляется только при клике на кнопку “редактировать”. 
<br/>3. При подтверждении - перезапись поста.
(Не забывайте про иммутабельный state)
<br/>4.Создайте коммит с номером урока и уровнем задания, запуште GitHub.</div>
      </div>
      <form ref="myForm" className="App__form" onSubmit={this.addTodo}>
          <input
          placeholder="Добавить новую лекцию"
            className="Crud__input"
            name="todo"
            ref="name"
          />
          <button className="App__form__button">Добавить</button>
        </form>
        <div >
          <LessonList
            list={todos}
            deleteTodo={this.deleteTodo}
            done={this.toggleDone}
            edit={this.editTodo}
          />
        </div>
    </div>

    
  );
}
}

export default СrudPage;