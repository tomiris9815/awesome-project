import React,{Component} from "react";
import Settings from "../../settings/Settings";
//import { Link } from "react-router-dom";
import LessonList from "../LessonList/LessonList";

import "./LessonPage2.scss";

class  СrudPage extends Component {
  state = {
    todos: [],
    somevalue: 456,
    act: 0,
    index: '',
  };

  addTodo = (e) =>{
    e.preventDefault();
    let todos = this.state.todos;
    let name = this.refs.name.value;

    if(this.state.act === 0){   
      let data = {
        name
      }
      todos.push(data);
     
    }else{                      
      let index = this.state.index;
      todos[index].name = name;   
    }    

    this.setState({
      todos: todos,
      act: 0
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  }
  
  deleteTodo = i => {
    this.setState(state => {
      state.todos.splice(i, 1);
      return { todos: state.todos };
    });
  };

  editTodo = (i) => {
    let data = this.state.todos[i];
    this.refs.name.value = data.name;

    this.setState({
      act: 1,
      index: i
    });

    this.refs.name.focus();
  } 
  
  toggleDone = i => {
    this.setState(state => {
      const { todos } = state;
      todos[i].done = !todos[i].done;

      return { todos };
    });
  };

  render(){ 
    const { todos } = this.state;
  return (
    <div className="Crud">
       <div className="Students__title">
      <b>Мои лекции</b>
       <div className="Students__back">
         <Settings />
        </div>
      </div>
      <div className="Crud__background">
      <div className="Crud__title">Урок 2 - Препроцессор JSX</div>
      <div className="Crud__lecture">Лекция 1</div>
      <div className="Crud__content">Дополнительные ссылки:
      &nbsp;<br />
•	ES6 классы - http://jsraccoon.ru/es6-classes
<br />
•	Babel - https://babeljs.io/
<br />
•	Webpack - https://webpack.js.org/
<br />
•	Create React App - КЛИК
<br />
•	Create React App - КЛИК
<br />
•	Создание шаблона с нуля - КЛИК
<br />
 <b>  ОБЯЗАТЕЛЬНОЕ ЗАДАНИЕ: 
 <br />
Продолжайте работать в той же папке. 
</b>
Возьмите выполненное домашнее задание из предыдущего урока. 
Его необходимо разделить на 3 файла.
<br />
1 файл: переменная employers, employersNames и манипуляции с ней.
<br />
2 файл: переменная sponsors, money и функция calcCash.
<br />
3 файл, результирующий: функцию makeBusiness превратить в класс, 
у которого будет метод для вывода всей информации в консоль.
<br />
 Здесь же создать экземпляр класса, который будет использовать полученные данные.
 Используйте babel и настройте его под IE 10.
Соберите файлы в сборку при помощи webpack.
Как итог - в консоль должен выводиться точно такой же результат, как и в начале. 

<br />
<b>УСЛОЖНЕННОЕ ЗАДАНИЕ:  </b>
<br />
1. Организовать этот проект, используя только классы. Также разделение на 3 файла.
<br />
2. В результирующем файле должны вызываться все необходимые конструкторы.
<br />
3. Как итог - в консоль должен выводиться точно такой же результат, как и в начале.
</div>
      </div>
      <form ref="myForm" className="App__form" onSubmit={this.addTodo}>
          <input
          placeholder="Добавить новую лекцию"
            className="Crud__input"
            name="todo"
            ref="name"
          />
          <button className="App__form__button">Добавить</button>
        </form>
        <div >
          <LessonList
            list={todos}
            deleteTodo={this.deleteTodo}
            done={this.toggleDone}
            edit={this.editTodo}
          />
        </div>
    </div>

    
  );
}
}

export default СrudPage;