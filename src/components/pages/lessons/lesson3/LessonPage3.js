import React,{Component} from "react";
import Settings from "../../settings/Settings";
//import { Link } from "react-router-dom";
import LessonList from "../LessonList/LessonList";

import "./LessonPage3.scss";

class  СrudPage extends Component {
  state = {
    todos: [],
    somevalue: 456,
    act: 0,
    index: '',
  };

  addTodo = (e) =>{
    e.preventDefault();
    let todos = this.state.todos;
    let name = this.refs.name.value;

    if(this.state.act === 0){   
      let data = {
        name
      }
      todos.push(data);
     
    }else{                      
      let index = this.state.index;
      todos[index].name = name;   
    }    

    this.setState({
      todos: todos,
      act: 0
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  }
  
  deleteTodo = i => {
    this.setState(state => {
      state.todos.splice(i, 1);
      return { todos: state.todos };
    });
  };

  editTodo = (i) => {
    let data = this.state.todos[i];
    this.refs.name.value = data.name;

    this.setState({
      act: 1,
      index: i
    });

    this.refs.name.focus();
  } 
  
  toggleDone = i => {
    this.setState(state => {
      const { todos } = state;
      todos[i].done = !todos[i].done;

      return { todos };
    });
  };

  render(){ 
    const { todos } = this.state;
  return (
    <div className="Crud">
       <div className="Students__title">
      <b>Мои лекции</b>
       <div className="Students__back">
         <Settings />
        </div>
      </div>
      <div className="Crud__background">
      <div className="Crud__title">Урок 3 - Знакомимся с React.js Стандарты ES6-8</div>
      <div className="Crud__lecture">Лекция 1</div>
      <div className="Crud__content">JSX – это препроцессор, который добавляет синтаксис XML 
          к JavaScript. Вы можете использовать React без JSX, но JSX делает React более элегантным.
          JSX – синтаксис, похожий на XML / HTML, используемый в React, расширяет ECMAScript,
          так что XML / HTML-подобный текст может сосуществовать с кодом JavaScript / React. 
          Синтаксис предназначен для использования препроцессорами (т. е. транспилерами, такими как Babel), 
          чтобы преобразовать HTML-подобный текст, найденный в файлах JavaScript, в стандартные объекты JavaScript,
          которые будут анализировать движок JavaScript.
          В основном, используя JSX, вы можете писать сжатые структуры HTML / XML (например,
          DOM подобные древовидные структуры) в том же файле, что и код JavaScript, а затем Babel преобразует эти выражения в код JavaScript.
          В отличие от прошлого, вместо того, чтобы помещать JavaScript в HTML, JSX позволяет нам помещать HTML в JavaScript
          <br /><b>  JSX – это выражение </b>
          <br />
            После компиляции выражения JSX становятся обычными объектами JavaScript.
            Это означает, что вы можете использовать JSX внутри операторов if или for, назначать его переменным,
            принимать его как аргументы и возвращать из функций
            <br /><b>Определение атрибутов в JSX:</b>
            <br />
          Вы можете использовать кавычки для указания строковых литералов в качестве атрибутов
          Вы также можете использовать фигурные скобки для вставки JavaScript в атрибут
          Не помещайте кавычки вокруг фигурных скобок выражения JavaScript. Вы должны либо использовать кавычки (для строковых значений), 
          либо фигурные скобки (для выражений), но не оба в одном и том же атрибуте.</div>
      </div>
      <form ref="myForm" className="App__form" onSubmit={this.addTodo}>
          <input
          placeholder="Добавить новую лекцию"
            className="Crud__input"
            name="todo"
            ref="name"
          />
          <button className="App__form__button">Добавить</button>
        </form>
        <div >
          <LessonList
            list={todos}
            deleteTodo={this.deleteTodo}
            done={this.toggleDone}
            edit={this.editTodo}
          />
        </div>
    </div>

    
  );
}
}

export default СrudPage;