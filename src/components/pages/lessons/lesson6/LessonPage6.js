import React,{Component} from "react";
import Settings from "../../settings/Settings";
//import { Link } from "react-router-dom";
import LessonList from "../LessonList/LessonList";

import "./LessonPage6.scss";

class  СrudPage extends Component {
  state = {
    todos: [],
    somevalue: 456,
    act: 0,
    index: '',
  };

  addTodo = (e) =>{
    e.preventDefault();
    let todos = this.state.todos;
    let name = this.refs.name.value;

    if(this.state.act === 0){   
      let data = {
        name
      }
      todos.push(data);
     
    }else{                      
      let index = this.state.index;
      todos[index].name = name;   
    }    

    this.setState({
      todos: todos,
      act: 0
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  }
  
  deleteTodo = i => {
    this.setState(state => {
      state.todos.splice(i, 1);
      return { todos: state.todos };
    });
  };

  editTodo = (i) => {
    let data = this.state.todos[i];
    this.refs.name.value = data.name;

    this.setState({
      act: 1,
      index: i
    });

    this.refs.name.focus();
  } 
  
  toggleDone = i => {
    this.setState(state => {
      const { todos } = state;
      todos[i].done = !todos[i].done;

      return { todos };
    });
  };

  render(){ 
    const { todos } = this.state;
  return (
    <div className="Crud">
       <div className="Students__title">
      <b>Мои лекции</b>
       <div className="Students__back">
         <Settings />
        </div>
      </div>
      <div className="Crud__background">
      <div className="Crud__title">Урок 4 - Знакомимся с React.js Стандарты ES6-8</div>
      <div className="Crud__lecture">Лекция 1</div>
      <div className="Crud__content"> <b>Дополнительные ссылки:</b> 
     <br/>
     •	React.Fragments:  https://reactjs.org/docs/fragments.html
<br/>
•	Sass-loader - https://github.com/webpack-contrib/sass-loader
<br/>
•	Sass in CRA - КЛИК
<br/>
•	Css-modules - https://habr.com/post/270103/
<br/>
•	Styled-components - https://www.styled-components.com/
<br/>

        <b>  Задание <br/>ОБЯЗАТЕЛЬНОЕ ЗАДАНИЕ: </b>
        <br/>
        1.Подключить bootstrap и styled-components.
        <br/>
2. Там, где возможно, использовать готовые компоненты bs.
 (компоненты post-status-filter и post-list-item не трогать)
<br/> 
3. Использовать styled-components для стилизации приложения.
(компоненты post-status-filter и post-list-item не трогать) 
 <br/>
 4.Сгенерировать уникальный id готовым инструментом наподобие: КЛИК
 <br/>
 5. Создайте коммит с номером урока и уровнем задания, запуште GitHub. 
 <br/>

          <b>УСЛОЖНЕННОЕ ЗАДАНИЕ:  </b>
          1. Создать механизм генерации уникальных случайных id для постов (числовой, буквенный или смешанный - на ваш выбор) 
<br/>2. При попытке удалить пост - выводить на экран модальное окно с вопросом “Вы точно собираетесь удалить запись?”.
 Окно создаете вы, alert не использовать.
<br/>3. При подтверждении - перезапись поста.
(Не забывайте про иммутабельный state)
<br/>4.Создайте коммит с номером урока и уровнем задания, запуште GitHub.</div>
      </div>
      <form ref="myForm" className="App__form" onSubmit={this.addTodo}>
          <input
          placeholder="Добавить новую лекцию"
            className="Crud__input"
            name="todo"
            ref="name"
          />
          <button className="App__form__button">Добавить</button>
        </form>
        <div >
          <LessonList
            list={todos}
            deleteTodo={this.deleteTodo}
            done={this.toggleDone}
            edit={this.editTodo}
          />
        </div>
    </div>

    
  );
}
}

export default СrudPage;