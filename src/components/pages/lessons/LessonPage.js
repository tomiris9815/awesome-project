import React,{Component} from "react";
import Settings from "../settings/Settings";
//import { Link } from "react-router-dom";
import LessonList from "./LessonList/LessonList";

import "./LessonPage.scss";

class  СrudPage extends Component {
  state = {
    todos: [],
    somevalue: 456,
    act: 0,
    index: '',
  };

  addTodo = (e) =>{
    e.preventDefault();
    let todos = this.state.todos;
    let name = this.refs.name.value;

    if(this.state.act === 0){   
      let data = {
        name
      }
      todos.push(data);
     
    }else{                      
      let index = this.state.index;
      todos[index].name = name;   
    }    

    this.setState({
      todos: todos,
      act: 0
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  }
  
  deleteTodo = i => {
    this.setState(state => {
      state.todos.splice(i, 1);
      return { todos: state.todos };
    });
  };

  editTodo = (i) => {
    let data = this.state.todos[i];
    this.refs.name.value = data.name;

    this.setState({
      act: 1,
      index: i
    });

    this.refs.name.focus();
  } 
  
  toggleDone = i => {
    this.setState(state => {
      const { todos } = state;
      todos[i].done = !todos[i].done;

      return { todos };
    });
  };

  render(){ 
    const { todos } = this.state;
  return (
    <div className="Crud">
       <div className="Students__title">
      <b>Мои лекции</b>
       <div className="Students__back">
         <Settings />
        </div>
      </div>
      <div className="Crud__background">
      <div className="Crud__title">Урок 1 - Знакомимся с React.js Стандарты ES6-8</div>
      <div className="Crud__lecture">Лекция 1</div>
      <div className="Crud__content">Ключевое слово let позволяет объявлять переменные 
      с ограниченной областью видимости — только для блока, в котором происходит объявление.
       Это называется блочной областью видимости. Вместо ключевого слова var,
       которое обеспечивает область видимости внутри функции, стандарт ES6 рекомендует использовать let.
       Другой формой объявления переменной с блочной областью видимости является ключевое слово const. 
       Оно предназначено для объявления переменных (констант), значения которых доступны только для чтения.
        Это означает не то, что значение константы неизменно,
        а то, что идентификатор переменной не может быть переприсвоен.
        <b>  2. Стрелочные функции </b>
          Стрелочные функции представляют собой сокращённую запись функций в ES6. 
          Стрелочная функция состоит из списка параметров ( ... ),
          за которым следует знак => и тело функции.
          <b>Это ещё не всё!...</b>
          Стрелочные функции не просто делают код короче. Они тесно связаны с ключевым словом this и привязкой контекста.
          Поведение стрелочных функций с ключевым словом this отличается от поведения обычных функций с this.
          Каждая функция в JavaScript определяет свой собственный контекст this, 
          но внутри стрелочных функций значение this то же самое, что и снаружи 
          (стрелочные функции не имеют своего this).
          В ECMAScript 3/5 это поведение стало возможным изменить, 
          присвоив значение this другой переменной.</div>
          <img className="lectureImage" alt= " "src={require('./lecture1.PNG')  }/>
      </div>
      <form ref="myForm" className="App__form" onSubmit={this.addTodo}>
          <input
          placeholder="Добавить новую лекцию"
            className="Crud__input"
            name="todo"
            ref="name"
          />
          <button className="App__form__button">Добавить</button>
        </form>
        <div >
          <LessonList
            list={todos}
            deleteTodo={this.deleteTodo}
            done={this.toggleDone}
            edit={this.editTodo}
          />
        </div>
    </div>

    
  );
}
}

export default СrudPage;