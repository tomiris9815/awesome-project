import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import LoginLayout from "./components/layouts/login-layout/LoginLayout";
import MainLayout from "./components/layouts/main-layout/MainLayout";
import RegisterLayout from "./components/layouts/reg-layout/RegisterLayout"
// import TeacherLayout from "./components/layouts/teacherLayout/TeacherLayout"

export default class App extends Component {
  render() {
    return (
      <Switch>   
        <Route  path="/auth" component={LoginLayout} /> 
        <Route  path="/register" component={RegisterLayout} /> 
        {/* <Route  path="/teacher" component={TeacherLayout} />  */}
        <Route path="/" component={MainLayout} />
      </Switch>
    );
  }
}

