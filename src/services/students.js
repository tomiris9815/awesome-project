import api from "./api";

export function getStudentsList() {
  return api.get("/users?page=2");
}

export function createUser(data) {
  return api.post("/user", data);
}
