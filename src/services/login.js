import api from "./api";

if (localStorage.getItem("access_token")) {
  api.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
    "access_token"
  )}`;
  
}

export function login(email, password) {
  return api.post("/login", { email, password });
}

