import { AUTH } from "../actions/auth.actions";
import {LOGIN} from "../actions/auth.actions"
const token =localStorage.getItem("access_token");
const username = token? token.split(":")[0]:"";

const initialState = {
    test: 7,
    username
};
export default function(state=initialState,action){
    switch(action.type){
        case LOGIN:
            return{
                ...state,
                email:action.payload
            } ;
        case AUTH:
            return{
                ...state,
                email:action.payload
            };
        case "LOGOUT":
            return{
                ...state
            };
        default:
            return state;
    }
}