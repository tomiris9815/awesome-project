import {combineReducers} from "redux";
import authReducer from "./auth.reducer";

const appReducer = combineReducers({
    user:authReducer
});

export default appReducer;
