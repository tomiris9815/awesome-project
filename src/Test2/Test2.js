import React from "react";

import "./TodoList.scss";

export default function TodoList({ list, deleteTodo, done }) {
  // ( props )
  return (
    <table className="App__todos w3-table w3-striped w3-bordered">
    <tbody >
      {list.map((todo, index) => (
        <tr
          key={index}
          className={`App__todo ${todo.done ? "App__todo--done" : ""} `}
        >
          <td>{todo.text}</td>
          <td className="App__todo__actions">
            <button className="App__toggle__button" onClick={() => done(index)}>Done</button>
            <button className="App__edit__button">Edit</button>
            <button className="App__delete__button"onClick={() => deleteTodo(index)}>Delete</button>
          </td>
        </tr>
      ))}
    </tbody>
    </table>
  );
}
