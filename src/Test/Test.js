import React, { Component } from "react";

export default class AddTodo extends Component {
  state = {
    inputValue: "",
    act: 0,
    index: '',
  };

  onInputChange = event => {
    this.setState({ inputValue: event.target.value });
  };
 khkjhkhjkh
  handleSubmit = event => {
    event.preventDefault();

    const newTodo = {
      text: this.state.inputValue,
      done: false
    };

    this.props.addAction(newTodo);
    this.setState({ inputValue: "" });
  };

  render() {
    const { inputValue } = this.state;

    return (
      <form ref="myForm" className="App__form" onSubmit={this.handleSubmit}>
        <input
          value={inputValue}
          onChange={this.onInputChange}
          className="App__input"
          name="todo"
        />
        <button className="App__form__button">Add</button>
      </form>
    );
  }
}
